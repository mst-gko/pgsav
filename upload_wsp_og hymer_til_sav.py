import os
import glob
import datetime

import pandas as pd
import psycopg2 as pg
import configparser
from sqlalchemy import create_engine


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    @property
    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials
        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                    host=cred['host'],
                    port=cred['port'],
                    database=cred['dbname'],
                    user=cred['usr'],
                    password=cred['pw']
            )
        except Exception as e:
            print(e)

        if cred:
            del cred

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')

        return df_pgsql

    def export_df_to_pg_db(self, df, schemaname, tablename):
        """
        Export pandas df to postgresDB
        :param df: pandas dataframe
        :param schemaname: database schema to export dataframe to
        :param tablename: database table to export dataframe to
        :return: None
        """
        cred = self.parse_db_credentials
        pgdatabase = cred['dbname']

        try:
            df.columns = df.columns.str.lower()
            pg_conn = f"postgresql+psycopg2://{cred['usr']}:{cred['pw']}@{cred['host']}:{cred['port']}/{cred['dbname']}"
            sql_engine = create_engine(pg_conn)
            del cred
            df.to_sql(tablename, con=sql_engine, schema=schemaname, if_exists='replace', index=False)
        except Exception as e:
            print('Table could not be edited: ' + pgdatabase + '.' + schemaname + '.' + tablename)
            print(e)

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """
        cred = self.parse_db_credentials
        pgdatabase = cred['dbname']
        try:
            with pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred


class UploadTilSav:
    def __init__(self, root_path):
        self.root_path = root_path

    @staticmethod
    def fix_name(filename_input):
        filename_input = filename_input.replace(' ', '_')
        filename_input = filename_input.replace('-', '_')
        filename_input = filename_input.replace('å', 'aa')
        filename_input = filename_input.replace('æ', 'ae')
        filename_input = filename_input.replace('ø', 'oe')
        filename_input = filename_input.replace('(', '')
        filename_input = filename_input.replace(')', '')
        filename_input = filename_input.lower()

        return filename_input

    def upload_xlsx(self):
        db = Database(database_name='GRUKOS', usr='writer')
        schema_name = 'sav'

        # loop through all xlsx files within root path (no subfolders)
        for filepath in glob.glob(f'{self.root_path}/*.xlsx'):
            print(f'\nfilename:{filepath}')

            # load xlsx file into pandas
            xls = pd.ExcelFile(filepath)

            # fetch filename from path
            base = os.path.basename(filepath)
            filename = os.path.splitext(base)[0]
            filename_fixed = self.fix_name(filename)

            # loop through all sheets within xlsx file
            for sheet_name in xls.sheet_names:
                print(f'...sheetname:{sheet_name}')
                df = pd.read_excel(xls, sheet_name)
                df['mst_insertdate'] = datetime.datetime.now()

                # combine xlsx filename and sheet name into database table name for export
                sheet_name_fix = self.fix_name(sheet_name)
                table_name = f'{filename_fixed}_{sheet_name_fix}'

                # crop tablename to be less than 65 chars (postgres rule)
                table_name = table_name[:63]

                # upload xlsx sheet to database
                db.export_df_to_pg_db(df=df, schemaname=schema_name, tablename=table_name)


# EDIT HERE: insert root folders for all xlsx files
# root_path = r'F:\GKO\data\Data til Grundvandsmodeller\01_Indput_data\Overfladevand\SaV'
#
# sav = UploadTilSav(root_path=root_path)
# sav.upload_xlsx()

path_hymer_vinge = r'F:\GKO\data\Data til Grundvandsmodeller\01_Indput_data\Overfladevand\SaV\AlleVinger29112022.txt'
df_hymer = pd.read_csv(path_hymer_vinge, sep='\t')
print(df_hymer.head())