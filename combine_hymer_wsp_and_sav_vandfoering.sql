CREATE SCHEMA IF NOT EXISTS mst_sav;

/*
 * vandføring alle
 */

BEGIN;

	DROP TABLE IF EXISTS mst_sav.vandfoering_all;
	
	CREATE TABLE mst_sav.vandfoering_all AS 
		(
			WITH 
				wsp2021_vandfoering AS 
					(
						SELECT 
							'WSP2021' AS data_source, 
							"name" AS navn,
							locality AS lokalitet,
							TO_TIMESTAMP(startdate, 'DD-MM-YYYY HH24.MI') AS tidspunkt,
							flow::NUMERIC AS vaerdi,
							NULL AS vaerditype,
							CASE 
								WHEN coordsysid = 2
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 4258), 25832) 
								WHEN coordsysid = 4
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25835), 25832)  
								WHEN coordsysid = 5
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25832), 25832)  
								WHEN coordsysid = 6
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25833), 25832) 
								WHEN coordsysid = 7
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 23032), 25832) 
								ELSE NULL
								END AS geom
						FROM sav_excelark.vingemaalinger_alle_1989_2021_fra_wsp_dec_2021_vandfoeringsmaal 
						WHERE flow IS NOT NULL 
					),	
				wsp2018_vandfoering AS 
					(
						SELECT 
							'WSP2018' AS data_source, 
							"name" AS navn,
							locality AS lokalitet,
							date_and_time AS tidspunkt,
							flow_liter_i_sec::NUMERIC AS vaerdi,
							NULL AS vaerditype,
							CASE 
								WHEN coordinate_system_id = 2
									THEN ST_Transform(ST_SetSRID(st_makepoint(x, y), 4258), 25832) 
								WHEN coordinate_system_id = 4
									THEN ST_Transform(ST_SetSRID(st_makepoint(x, y), 25835), 25832)  
								WHEN coordinate_system_id = 5
									THEN ST_Transform(ST_SetSRID(st_makepoint(x, y), 25832), 25832)  
								WHEN coordinate_system_id = 6
									THEN ST_Transform(ST_SetSRID(st_makepoint(x, y), 25833), 25832) 
								WHEN coordinate_system_id = 7
									THEN ST_Transform(ST_SetSRID(st_makepoint(x, y), 23032), 25832) 
								END AS geom
						FROM sav_excelark.wsp_data_lev_2018_oprindelig 
						WHERE flow_liter_i_sec IS NOT NULL 
					),
				hymer_vandfoering AS 
					(
						SELECT 
							'Hymer' AS data_source,
							lok."name" AS navn,
							lok.locality AS lokalitet,
							TO_TIMESTAMP(dat.startdate, 'DD-MM-YYYY HH24:MI:SS') AS tidspunkt,
							dat.flow::NUMERIC AS vaerdi,
							NULL AS vaerditype,
							CASE 
								WHEN coordsys = 2
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 4258), 25832) 
								WHEN coordsys = 4
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25835), 25832)  
								WHEN coordsys = 5
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25832), 25832)  
								WHEN coordsys = 6
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 25833), 25832) 
								WHEN coordsys = 7
									THEN ST_Transform(ST_SetSRID(st_makepoint(coordx, coordy), 23032), 25832) 
								ELSE NULL 
								END AS geom
						FROM sav.hymer_cumecadm_edit dat 
						INNER JOIN sav.hymer_location lok USING (id)
						INNER JOIN sav.hymer_dmuobsnrtab info USING (id)	
						WHERE flow IS NOT NULL 
					),
				sav_vandfoering AS 
					(
						SELECT 
							'Sav' AS data_source,
							navn,
							lokalitet,
							CASE
								WHEN info.tidszone = 'UTC +1 (dansk normaltid)' 
									THEN TO_TIMESTAMP(REPLACE(dat.tidspunkt, 'T', ' '), 'YYYY-MM-DD HH24:MI:SS')
								WHEN info.tidszone = 'UTC'
									THEN to_timestamp(REPLACE(dat.tidspunkt, 'T', ' '), 'YYYY-MM-DD HH24:MI:SS') at time zone (select current_setting('timezone')) at time zone 'UTC' 
								ELSE TO_TIMESTAMP(REPLACE(dat.tidspunkt, 'T', ' '), 'YYYY-MM-DD HH24:MI:SS') 
								END AS tidspunkt,
							REPLACE(NULLIF(vaerdi, 'None'), ',', '.')::NUMERIC AS vaerdi,
							vaerditype,
							CASE 
								WHEN projektion = 'UTM Zone 32 ETRS89'
									THEN ST_Transform(ST_SetSRID(st_makepoint(positionx, positiony), 25832), 25832)  
								ELSE NULL
								END AS geom
						FROM sav.sav_timeseries dat 
						INNER JOIN sav.sav_timeseries_info info USING (tsid)
						INNER JOIN sav.sav_locations lok USING (ejerlokid)
						WHERE info."parameter" = 'Vandføring'
							AND NULLIF(vaerdi, 'None') IS NOT NULL 
					)
			SELECT *
			FROM wsp2021_vandfoering 
			UNION ALL 
			SELECT *
			FROM wsp2018_vandfoering 
			UNION ALL 
			SELECT *
			FROM hymer_vandfoering 
			UNION ALL 
			SELECT *
			FROM sav_vandfoering 
		)
	;

COMMIT; 

/*
 * vandføring unik
 */

BEGIN;

	DROP TABLE IF EXISTS mst_sav.vandfoering_unique;
	
	CREATE TABLE mst_sav.vandfoering_unique AS 
		(
			SELECT DISTINCT ON (navn, lokalitet, geom, tidspunkt)
				*
			FROM mst_sav.vandfoering_all
			ORDER BY navn, lokalitet, geom, tidspunkt,
				(
					CASE 
						WHEN data_source = 'Sav'
							THEN 1
						WHEN data_source = 'Hymer'
							THEN 2
						WHEN data_source = 'WSP2021'
							THEN 3
						WHEN data_source = 'WSP2018'
							THEN 4
						END 
				)
		)
	;

COMMIT;

/*
 * stationer alle
 */

BEGIN; 

	DROP TABLE IF EXISTS mst_sav.stationer_all;
	
	CREATE TABLE mst_sav.stationer_all AS 
		(
			SELECT  
				data_source,
				navn,
				lokalitet,
				min(tidspunkt) AS tidspunkt_min,
				max(tidspunkt) AS tidspunkt_max,
				min(vaerdi) AS vaerdi_min,
				max(vaerdi) AS vaerdi_max,
				avg(vaerdi) AS vaerdi_avg,
				count(*) AS antal_maalinger,
				geom 
			FROM mst_sav.vandfoering_all
			GROUP BY data_source,
				navn,
				lokalitet,
				geom
		)
	;

COMMIT;

/*
 * stationer unik
 */

BEGIN;

	DROP TABLE IF EXISTS mst_sav.stationer_unique;
	
	CREATE TABLE mst_sav.stationer_unique AS 
		(
			SELECT  
				data_source,
				navn,
				lokalitet,
				min(tidspunkt) AS tidspunkt_min,
				max(tidspunkt) AS tidspunkt_max,
				min(vaerdi) AS vaerdi_min,
				max(vaerdi) AS vaerdi_max,
				avg(vaerdi) AS vaerdi_avg,
				count(*) AS antal_maalinger,
				geom 
			FROM mst_sav.vandfoering_unique
			GROUP BY data_source,
				navn,
				lokalitet,
				geom
		)
	;

COMMIT;
		
GRANT USAGE ON SCHEMA sav TO grukosreader;
GRANT USAGE ON SCHEMA mst_sav TO grukosreader;
GRANT USAGE ON SCHEMA sav_excelark TO grukosreader;

GRANT SELECT ON ALL TABLES IN SCHEMA sav TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA mst_sav TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA sav_excelark TO grukosreader;