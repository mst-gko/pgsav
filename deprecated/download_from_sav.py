import os
# import datetime

import wget
import pandas as pd
import configparser
from sqlalchemy import create_engine
import psycopg2 as pg


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def fetch_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def fetch_pg_engine(self):
        """
        Connects to postgresql server database
        :return: postgresql sqlalchemy connection engine and string database name
        """
        cred = self.fetch_db_credentials()

        pg_database_name = cred['dbname']
        pg_conn_str = f"postgresql+psycopg2://{cred['usr']}:{cred['pw']}@{cred['host']}:{cred['port']}/{cred['dbname']}"
        pg_engine = create_engine(pg_conn_str)
        del cred

        return pg_engine, pg_database_name

    def export_df_to_pg_db(self, df, schemaname, tablename, if_exists='replace'):
        """
        Export pandas df to postgresDB
        :param df: pandas dataframe
        :param schemaname: database schema to export dataframe to
        :param tablename: database table to export dataframe to
        :param if_exists: how to act if table exists in database: 'replace' (default) or 'append'
        :return: None
        """

        sql_engine, pgdatabase = self.fetch_pg_engine()
        try:
            df.columns = df.columns.str.lower()
            df.to_sql(tablename, con=sql_engine, schema=schemaname, if_exists=if_exists, index=False)
        except Exception as e:
            print('Table could not be edited: ' + pgdatabase + '.' + schemaname + '.' + tablename)
            print(e)

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        sql_engine, pgdatabase = self.fetch_pg_engine()
        conn = sql_engine.connect()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {pgdatabase}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql


def fetch_filename(csv_name):
    """
    outputs filename from path
    :param csv_name: path to csv file [str]
    :return: filename [str]
    """
    # fetch filename without file-extension from path
    base = os.path.basename(csv_name)
    filename = os.path.splitext(base)[0]

    return filename


def download_and_export(url, csv_name, table_name, schema_name, if_exists='replace'):
    """
    download csv, import csv as df and export df to grukos.
    The function downloads a csv file instaed of using a get command to fetch a json response,
    since the latter didnt work properly at the time of creating this script.
    see: https://hydrometri-api.miljoeportal.dk/swagger/index.html
    :param url:
    :param csv_name:
    :param table_name:
    :param schema_name:
    :param if_exists:
    :return:
    """
    # remove csv file if exists
    if os.path.exists(csv_name):
        os.remove(csv_name)

    # download csv
    wget.download(url, out=csv_name, bar=None)

    # load csv into pandas and export data
    df = pd.read_csv(csv_name, delimiter=';')
    # df['mst_insertdate'] = datetime.datetime.now()
    db.export_df_to_pg_db(df=df, schemaname=schema_name, tablename=table_name, if_exists=if_exists)

    # remove csv file if exists
    if os.path.exists(csv_name):
        os.remove(csv_name)


def download_and_export_timeseries(url, csv_name, table_name, schema_name, uuid_timeseries, if_exists='replace'):
    """
    download csv, import csv as df and export df to grukos.
    The function downloads a csv file instaed of using a get command to fetch a json response,
    since the latter didnt work properly at the time of creating this script.
    see: https://hydrometri-api.miljoeportal.dk/swagger/index.html
    separate function for timeseries since csv is lacking needed data
    :param url:
    :param csv_name:
    :param table_name:
    :param schema_name:
    :param uuid_lokation:
    :param uuid_timeseries:
    :param if_exists:
    :return:
    """
    # remove csv file if exists
    if os.path.exists(csv_name):
        os.remove(csv_name)

    # download csv
    wget.download(url, out=csv_name, bar=None)

    # load csv into pandas and export data
    df = pd.read_csv(csv_name, delimiter=';')
    df['tsid'] = uuid_timeseries
    df['vaerdi'] = df['Værdi']
    df.pop('Værdi')

    db.export_df_to_pg_db(df=df, schemaname=schema_name, tablename=table_name, if_exists=if_exists)

    # remove csv file if exists
    if os.path.exists(csv_name):
        os.remove(csv_name)


# set vars
schema_name = 'sav'
table_name_prefix = 'sav'
db = Database(database_name='GRUKOS', usr='writer')

# download locations and export to grukos db
print('\n\nloading locations...')
csv_name_locations = 'locations.csv'
filename_locations = fetch_filename(csv_name_locations)
table_name_locations = f'{table_name_prefix}_{filename_locations}'
url_locations = 'https://hydrometri-api.miljoeportal.dk/api/locations/file'
download_and_export(
    url=url_locations,
    csv_name=csv_name_locations,
    table_name=table_name_locations,
    schema_name=schema_name
)
print('\nlocations loaded.')

# download timeseries information and export to grukos db
print('\n\nloading timeseries_info...')
csv_name_timeseries_info = 'timeseries.csv'
filename_timeseries_info = fetch_filename(csv_name_timeseries_info)
table_name_timeseries_info = f'{table_name_prefix}_{filename_timeseries_info}_info'
url_timeseries_info = 'https://hydrometri-api.miljoeportal.dk/api/locations/timeseries/file'
download_and_export(
    url=url_timeseries_info,
    csv_name=csv_name_timeseries_info,
    table_name=table_name_timeseries_info,
    schema_name=schema_name
)
print('\ntimeseries_info loaded.')

# loop through all timeseries uuid from timeseries_info (see above) and export to grukos db
print('\n\nloading timeseries...')
table_name_timeseries = f'{table_name_prefix}_timeseries'
sql_timeseries = f'''
    SELECT DISTINCT  
        ejerlokid AS uuid_lokation, 
        tsid AS uuid_timeseries
    FROM {schema_name}.{table_name_timeseries_info}
    WHERE 
        (
            "parameter" = 'Vandføring'
                AND vaerditype IN ('Døgnmiddel', 'Øjebliksværdier') 
                AND serietype IN ('Beregnede kvalitetssikrede data', 'Kvalitetssikrede rådata')
        )
        OR 
        (
            "parameter" = 'Vandstand'
                AND vaerditype IN ('Døgnmiddel', 'Øjebliksværdier') 
                AND serietype = 'Kvalitetssikrede rådata' 
        )
    ;
'''
df_uuid_timeseries = db.import_pgsql_to_df(sql_timeseries)
number_total = df_uuid_timeseries.shape[0]

for i, (index, row) in enumerate(df_uuid_timeseries.iterrows()):
    #print(f'\r\t- {i+1} out of {number_total}...', end='', flush=True)
    print(f'\t- {i+1} out of {number_total}...')
    uuid_lokation = row['uuid_lokation']
    uuid_timeseries = row['uuid_timeseries']
    csv_name_timeseries = f'data-{uuid_timeseries}.csv'
    filename_timeseries = fetch_filename(csv_name_locations)
    url_timeseries_info = f'https://hydrometri-api.miljoeportal.dk/api/locations/{uuid_lokation}/timeseries/{uuid_timeseries}/data/file'

    if i > 0:
        if_exists = 'append'
    else:
        if_exists = 'replace'

    download_and_export_timeseries(
        url=url_timeseries_info,
        csv_name=csv_name_timeseries,
        table_name=table_name_timeseries,
        schema_name=schema_name,
        uuid_timeseries=uuid_timeseries,
        if_exists=if_exists
    )
print('\ntimeseries loaded.')
